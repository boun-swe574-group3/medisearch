# FROM python:3.9.4
# ENV PYTHONUNBUFFERED=1
# WORKDIR /app
# COPY . /app/
# RUN pip install -r requirements.txt
# EXPOSE 8000
# CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "config.wsgi:application"]

FROM python:3.9.4
ENV PYTHONUNBUFFERED=1
WORKDIR /app
ADD ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
ADD . /app/
# COPY requirements.txt requirements.txt

EXPOSE 8000
CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "config.wsgi:application"]