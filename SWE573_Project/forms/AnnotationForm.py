from django import forms
from SWE573_Project.models import ArticleAnnotation


class AnnotationForm(forms.ModelForm):
    # url = forms.CharField(widget=forms.HiddenInput(), required=True)

    class Meta:
        model = ArticleAnnotation
        # fields = ["start_index", "end_index"]
        exclude = ["user", "created"]

    def save(self, commit=True, **kwargs):
        m = super(AnnotationForm, self).save(commit=False)
        # do custom stuff
        if commit:
            m.user_id = kwargs.get('user_id')
            m.save()
        return m
