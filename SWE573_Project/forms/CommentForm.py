# from django import forms
# from django.forms import widgets
# from SWE573_Project.models.ArticleAnnotation import ArticleAnnotation
#
#
# # Allow user to make their own annotation
# class CommentForm(forms.ModelForm):
#     class Meta:
#         model = ArticleAnnotation
#         fields = ['body']
#
#         widgets = {
#             'body': forms.Textarea(attrs={'class': 'form-control'})
#         }
#
# class AnnotateForm(forms.ModelForm):
#     class Meta:
#         model = ArticleAnnotation
#         fields = ['article', 'tag']
#
#         widgets = {
#             'target': forms.Textarea(attrs={'class': 'form-control'})
#         }