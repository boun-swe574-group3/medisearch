from django.urls import path, include
from SWE573_Project.views import mainpage
from SWE573_Project.views import tag, tag_ajax
from SWE573_Project.views import details
from SWE573_Project.views import report
from SWE573_Project.views import advancedSearch
# from SWE573_Project.views.comment import AddCommentView, CommentListView, AddAnnotateView
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from SWE573_Project.views import search
from SWE573_Project.views import DetailsTag


urlpatterns = [
    path('login', auth_views.LoginView.as_view(
        template_name='pages/login.html'
    ), name='login'),
    path('aboutus', TemplateView.as_view(
        template_name='pages/aboutus.html'
    ), name='aboutus'),
    path('', mainpage, name='homepage'),
    path('tag/<int:article_id>', tag, name='tag'),
    path('tag_ajax/<int:article_id>', tag_ajax, name='tag_ajax'),
    path('details/<slug:slug>', details.as_view(), name='details'),
    # path('details/<int:article_id>/newcomment', AddCommentView.as_view(template_name='pages/add_comment.html'), name='add_comment'),
    # path('details/<int:article_id>/comments', CommentListView.as_view(template_name='pages/comment.html'), name='comment'),
    # path('details/<int:article_id>/newannotation',AddAnnotateView.as_view(template_name='pages/annotate.html'), name='annotate'),
    path('report/<int:article_id>', report, name='report'),
    path('search', search, name='search'),
    path('', include('account.urls')),


    path('advancedsearch', advancedSearch, name='advancedSearch'),

]
