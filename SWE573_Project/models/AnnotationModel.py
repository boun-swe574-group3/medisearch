from django.db import models
from django.db.models import JSONField


class AnnotateMed(models.Model):
    # motivation_choices = (('1', 'Bookmarking'),
    #                       ('2', 'Highlighting'),
    #                       ('3', 'Tagging'),
    #                       ('4', 'Commenting'),
    #                       ('5', 'Questioning'))
    # motive = models.TextField(max_length=15, choices=motivation_choices)
    annotation_json = JSONField()
    # annotation_json = models.TextField(null=True, blank=True)

