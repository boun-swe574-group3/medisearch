from django.db import models
from autoslug import AutoSlugField


from SWE573_Project.models import TagModel, ArticleAnnotation
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

from SWE573_Project.models import AnnotationModel
# from datetime import datetime


class ArticleModel (models.Model):
    title = models.TextField(max_length=1000)
    body = RichTextField()
    authors = models.TextField(max_length=20000)
    pmid = models.TextField(max_length=1000, unique=True)
    # pubmed tarihini çekmek araştırılacak
    created_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now_add=True)
    publish_date = models.DateField(null=True)
    keywords = models.TextField(max_length=1000)
    slug = AutoSlugField(populate_from='title', unique=True)
    # Connect tags with annotation
    tags = models.ManyToManyField(
        TagModel, related_name='article')
    annotation = models.ManyToManyField(
        AnnotationModel.AnnotateMed, related_name='annotations')
    article_ann = models.ManyToManyField(ArticleAnnotation.ArticleAnnotation, related_name="article_ann")

    class Meta:
        db_table = 'articles'
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'

    def __str__(self):
        return self.title
