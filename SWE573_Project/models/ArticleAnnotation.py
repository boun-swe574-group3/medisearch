from django.db import models
from SWE573_Project.models import TagModel, ArticleModel
from django.utils import timezone
from django.urls import reverse


class ArticleAnnotation(models.Model):
    # Defines the area selected by the user to annotate
    target = models.ForeignKey(TagModel, null=True, on_delete=models.CASCADE)
    # article = models.ForeignKey(ArticleModel, on_delete=models.CASCADE)  # Source of the annotation is PubMed article
    # tag = models.ForeignKey(TagModel, null=True, on_delete=models.CASCADE)  # body of the annotation will be Wikidata tag
    start_index = models.IntegerField(null=True)
    end_index = models.IntegerField(null=True)
    body = models.TextField(null=True, verbose_name='body')
    created = models.DateTimeField(default=timezone.now)


    # def __str__(self):
    #     return self.body

    def get_absolute_url(self):
        return reverse('comment', kwargs={'article_id': self.article_id})
