const selectableTextArea = document.querySelectorAll('.selectable-text-area');
const plusBtn = document.querySelector('#plus-btn');
const tagBtn = document.querySelector('#tag-btn');
//const likeBtn = document.querySelector(".like__btn");
let likeIcon = document.querySelector('#likeicon');
let countup = document.querySelector('#countup');
const dislikeBtn = document.querySelector('.dislike__btn');
let dislikeIcon = document.querySelector('#dislikeicon');
let countdown = document.querySelector('#countdown');
const tagannBtn = document.querySelector('#tag_ann');

/*check if text is selected*/
selectableTextArea.forEach((elem) => {
  elem.addEventListener('mouseup', selectableTextAreaMouseUp);
});

let tagObj = {};

$(function () {
  var availableTags = [];

  var link =
    'https://www.wikidata.org/w/api.php?action=wbsearchentities&format=json&errorformat=plaintext&language=en&uselang=en&type=item';

  $('#tag_ann').autocomplete({
    source: function (request, response) {
      $.ajax({
        url: link,
        dataType: 'jsonp',
        data: {
          search: request.term,
        },
        success: function (data) {
          var searchTerms = [];
          data.search.forEach((element) => {
            searchTerms.push({
              id: element.id,
              label: element.label + ' | ' + element.description,
              value: element.label,
              desc: element.description,
              url: element.url,
            });
          });
          response(searchTerms);
        },
      });
    },
    minLength: 2,
    select: function (event, ui) {
      tagObj.url = 'https://' + ui.item.url.replace('//', '');
      $('#tag_ann').val(ui.item.desc);
      tagObj.name = ui.item.value;
      //console.log(tagObj);
    },
  });
});

//add annotate and highlight buttons near selected text
function selectableTextAreaMouseUp(event) {
  /*   const selectedText = window.getSelection().toString().trim();
        console.log(selectedText);   */
  setTimeout(() => {
    const selectedText = window.getSelection().toString().trim();
    if (selectedText.length) {
      const x = event.pageX;
      const y = event.pageY;
      const plusBtnWidth = Number(getComputedStyle(plusBtn).width.slice(0, -2));
      const plusBtnHeight = Number(getComputedStyle(plusBtn).height.slice(0, -2));
      plusBtn.style.left = `${x - 20}px`;
      plusBtn.style.top = `${y + 5}px`;
      plusBtn.style.display = 'block';
      plusBtn.classList.add('btnEntrance');
      tagBtn.style.left = `${x - 20 - plusBtnWidth}px`;
      tagBtn.style.top = `${y + 5}px`;
      tagBtn.style.display = 'block';
      tagBtn.classList.add('tagBtnEntrance');
      tagannBtn.style.display = 'block';
      tagannBtn.style.left = `${x - 20 - plusBtnWidth}px`;
      tagannBtn.style.top = `${y - plusBtnHeight}px`;
    }
  }, 0);
}

document.addEventListener('mousedown', documentMouseDown);

function documentMouseDown(event) {
  if (
    getComputedStyle(plusBtn).display === 'block' &&
    event.target.id !== 'plus-btn' &&
    event.target.id !== 'tag_ann' &&
    event.target.classList &&
    !event.target.classList.includes('ui-autocomplete')
  ) {
    plusBtn.style.display = 'none';
    tagannBtn.style.display = 'none';
    tagBtn.style.display = 'none';
    plusBtn.classList.remove('btnEntrance');
    // highlightBtn.classList.remove("highBtnEntrance");
    tagBtn.classList.remove('tagBtnEntrance');
    window.getSelection().empty();
  }
}





// function plusBtnClick(event) {
//   plusBtn.style.display = 'none';
//   tagBtn.style.display = 'none';
//   tagannBtn.style.display = 'none';
//   plusBtn.classList.remove('btnEntrance');
//   tagBtn.classList.remove('tagBtnEntrance');
//   tagBtn.classList.remove('tagBtnEntrance');
//   const text = window.getSelection().toString().trim();
// }

let selectedText = '';
let startIndex = 0;
let endIndex = 0;

let fullText = document.getElementById('full-text');
let annotateButton = document.getElementById('plus-btn');

fullText.addEventListener('mouseup', () => {
  if (window.getSelection) {
    let selection = window.getSelection();
    selectedText = selection.toString();
    if (selection.anchorOffset < selection.focusOffset) {
      startIndex = selection.anchorOffset;
      console.log(startIndex);
      endIndex = selection.focusOffset;
      console.log(endIndex);
    } else {
      startIndex = selection.focusOffset;
      endIndex = selection.anchorOffset;
    }
  }
});

function highlightTarget(startIndex, endIndex) {
  let target = document.getElementById('paragraph')
  let targetInnerHTML = target.innerText;
  if (startIndex >= 0) {
    targetInnerHTML = targetInnerHTML.substring(0, startIndex) + "<span class='highlight'>" + targetInnerHTML.substring(startIndex, endIndex) + '</span>' + targetInnerHTML.substring(endIndex);
    target.innerHTML = targetInnerHTML
  }
  //highlightTarget(selectedText)
}

annotateButton.addEventListener('click', () => {
  document.getElementById('user-selection').style.display = 'block';
  document.getElementById('text-selection').value = selectedText;
  document.getElementById('start-index').value = startIndex;
  console.log(startIndex)
  document.getElementById('end-index').value = endIndex;
  highlightTarget(startIndex, endIndex);


  const slug = document.getElementById('slug').innerHTML;
  const article_id = document.getElementById('article_id').innerHTML;
  const csrf_token = document.querySelector("input[name='csrfmiddlewaretoken']").value;

  $.post(
    '/tag_ajax/' + article_id, // url
    {
      name: tagObj.name,
      url: tagObj.url,
      csrfmiddlewaretoken: csrf_token,
    }, // data to be submit

    function (data, status, jqXHR) {
      $.post(
        '/details/' + slug, // url
        {
          target: data.id,
          start_index: startIndex,
          end_index: endIndex,
          body: selectedText,
          csrfmiddlewaretoken: csrf_token,

        },
        function (data, status, jqXHR) {
          highlightTarget(startIndex, endIndex)
          location.reload();

          // let split_text = document.getElementById('paragraph').split();
          // let splitHTML = '';
          // split_text.forEach(function (result) {
          //   splitHTML += '<span class=' + result + '>' + result + '</span>';
          // });
          // document.getElementById('paragraph').appendChild(splitHTML);
        },
      );
      console.log(startIndex, "START INDEX IS")// data to be submit

      startIndex = 0;
      endIndex = 0;
      });
    }
  );



// if highlight button is clicked, selected text is highlighted
//highlightBtn.addEventListener("click", highlightBtnClick);

// function highlightBtnClick(event) {
//   const text = window.getSelection().toString().trim();
//   const startIndex = text.focusOffset;
//   const endIndex = text.anchorOffset;
//   const inputText = document.getElementById('paragraph');
//   let innerHTML = inputText.innerHTML;
//   if (startIndex >= 0) {
//     innerHTML =
//       innerHTML.substring(0, startIndex) +
//       "<span class='highlight'>" +
//       innerHTML.substring(startIndex, endIndex) +
//       '</span>' +
//       innerHTML.substring(endIndex, inputText.length);
//     inputText.innerHTML = innerHTML;
//   }
// }

// Annotated text remains highlighted
//annotateBtn.addEventListener("click", annotateBtnClick);

// function annotateBtnClick(event) {
//     const text = window.getSelection().toString().trim();
//     const startIndex = text.focusOffset;
//     const endIndex = text.anchorOffset;
//     const inputText = document.getElementById("paragraph");
//     const slug = document.getElementById("slug");
//     let innerHTML = inputText.innerHTML;
//     if (startIndex >= 0) {
//         innerHTML = innerHTML.substring(0, startIndex) + "<span class='highlight'>" + innerHTML.substring(startIndex, endIndex) + "</span>" + innerHTML.substring(endIndex, inputText.length);
//         inputText.innerHTML = innerHTML;
//     }
//     $.post('/details/' + slug,   // url
//         {
//             target: inputText,
//             startIndex: startIndex,
//             endIndex: endIndex,
//             url: '',
//             body: text,
//         }, // data to be submit
//         function (data, status, jqXHR) {// success callback
//             location.reload();
//         })

// }

//Like and dislike buttons for comments
let clicked = false;

// likeBtn.addEventListener("click", () => {
//     if (!clicked) {
//         clicked = true;
//         likeIcon.innerHTML = `<i class="fas fa-thumbs-up"></i>`
//         countup.textContent++;
//     }
//     else {
//         clicked = false;
//         likeIcon.innerHTML = `<i class="far fa-thumbs-up"></i>`
//         countup.textContent--;
//     }
// });

let clickeddown = false;

// dislikeBtn.addEventListener("click", () => {
//     if (!clickeddown) {
//         clickeddown = true;
//         dislikeIcon.innerHTML = `<i class="fas fa-thumbs-down"></i>`
//         countdown.textContent++;
//     }
//     else {
//         clickeddown = false;
//         dislikeIcon.innerHTML = `<i class="far fa-thumbs-down"></i>`
//         countdown.textContent--;
//     }
// });
