from .mainpage import mainpage
from .details import details
from .report import report
from .login import login
from .createnewuser import createnewuser
from .tag import tag, tag_ajax
from .advancedSearch import advancedSearch
from .searchresult import search
from .detailstag import DetailsTag
from .annotator import annotate
# from .annotations import annotations
