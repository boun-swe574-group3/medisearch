from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from SWE573_Project.models import ArticleModel, TagModel, ActivityModel
from SWE573_Project.forms import TagForm
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from account.models import CustomUserModel
import datetime
import json
from django.core import serializers


@login_required(login_url="/login")
def tag(request, article_id):
    form = TagForm()

    if request.method == 'POST':
        form = TagForm(request.POST)
        print(form)
        if form.is_valid():
            user_id = request.user.id
            cleaned_info = form.cleaned_data
            i = TagModel.objects.filter(name=cleaned_info['name'])
            if not i.exists():
                i = form.save(user_id=user_id, article_id=article_id)
            else:
                i = i.first()
            article = ArticleModel.objects.get(id=article_id)
            article.tags.add(i)
            user = CustomUserModel.objects.get(id=user_id)

            # ACTIVITY SAVER FOR TAGGING
            createdTime = datetime.datetime.now()
            message = f"{user} tagged the article '{article}' with the tag of '{i.name}' at {createdTime}"
            articleLink = f"http://18.222.146.15:80/details/{article.slug}"
            userLink = f"http://18.222.146.15:80/profiles/{user.profile.id}"


            json_activity = {
                "@context": "https://www.w3.org/ns/activitystreams",
                "summary": message,
                "type": "Add",
                "published": str(createdTime),
                "actor": {
                    "type": "Person",
                    "name": str(request.user),
                    "url": userLink
                },
                "object": {
                    "type": "Page",
                    "name": str(i.name),
                    "url": str(i.url)
                },
                "target": {
                    "type": "Article",
                    "name": article.title,
                    "content": article.body,
                    "attributedTo": articleLink
                }
            }

            print(type(json_activity))

            activity = json.dumps(json_activity)
            print(activity)  # double quotes ""
            loaded_activity = json.loads(activity)
            print(type(loaded_activity))

            newAnnotationactivity = ActivityModel.Activity.objects.create(activity_JSON=json_activity)
            newAnnotationactivity.save()
            user.profile.activities.add(newAnnotationactivity)





        # message = f"{user} tagged {article} with the tag of {i}"
        # created = datetime.datetime.now
        #
        # json = {
        #     "@context": "https://www.w3.org/ns/activitystreams",
        #     "summary": message,
        #     "type": "Create",
        #     "id": "http://www.test.example/activity/1",
        #     "actor": "http://example.org/profiles/joe",
        #     "object": "http://example.com/notes/1",
        #     "published": created
        # }

        # response = json.dumps(activity)
        # response = serializers.serialize('json', activity)
        # print(f"{response} IS THE RESPONSE")
        #activity = Activity(user=user, activity_JSON=json)
        # activity.save()

        # json = {
        #     "@context": "https://www.w3.org/ns/activitystreams",
        #     "summary": "{} followed {}".format(self.getOwnerName(), activity_target_name),
        #     "type": "Follow",
        #     "published": self.getCurrentTimeAsISO(),
        #     "actor": {
        #         "type": "Person",
        #         "id": self.getOwnerURL(),
        #         "name": self.getOwnerName(),
        #         "url": self.getOwnerURL()
        #     },
        #     "object": {
        #         "id": activity_target_url,
        #         "type": activity_target_type,
        #         "url": activity_target_url,
        #         "name": activity_target_name
        #     }
        # }

        # activity = Activity(
        #     user_id=self.user_id,
        #     activity_type=2,
        #     target_type=1,
        #     target_id=target_id,
        #     activity_JSON=json
        # )
        # activity.save()

        return redirect('details', article.slug)

    context = {
        'form': form,
    }
    return render(request, 'pages/tag.html', context=context)


@login_required(login_url="/login")
def tag_ajax(request, article_id):
    form = TagForm()

    if request.method == 'POST':
        form = TagForm(request.POST)
        print(form)
        i = None
        if form.is_valid():
            user_id = request.user.id
            cleaned_info = form.cleaned_data
            i = TagModel.objects.filter(name=cleaned_info['name'])
            if not i.exists():
                i = form.save(user_id=user_id, article_id=article_id)
            else:
                i = i.first()
            article = ArticleModel.objects.get(id=article_id)
            article.tags.add(i)
            user = CustomUserModel.objects.get(id=user_id)
        message = f"{user} tagged {article} with the tag of {i}"
        created = datetime.datetime.now

        json = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "summary": message,
            "type": "Create",
            "id": "http://www.test.example/activity/1",
            "actor": "http://example.org/profiles/joe",
            "object": "http://example.com/notes/1",
            "published": created
        }

        # response = json.dumps(activity)
        # response = serializers.serialize('json', activity)
        # print(f"{response} IS THE RESPONSE")
        #activity = Activity(user=user, activity_JSON=json)
        # activity.save()

        # json = {
        #     "@context": "https://www.w3.org/ns/activitystreams",
        #     "summary": "{} followed {}".format(self.getOwnerName(), activity_target_name),
        #     "type": "Follow",
        #     "published": self.getCurrentTimeAsISO(),
        #     "actor": {
        #         "type": "Person",
        #         "id": self.getOwnerURL(),
        #         "name": self.getOwnerName(),
        #         "url": self.getOwnerURL()
        #     },
        #     "object": {
        #         "id": activity_target_url,
        #         "type": activity_target_type,
        #         "url": activity_target_url,
        #         "name": activity_target_name
        #     }
        # }

        # activity = Activity(
        #     user_id=self.user_id,
        #     activity_type=2,
        #     target_type=1,
        #     target_id=target_id,
        #     activity_JSON=json
        # )
        # activity.save()

    context = {
        'id': i.id,
    }
    return JsonResponse(context)
