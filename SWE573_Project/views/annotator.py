from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404

from SWE573_Project.forms import CommentForm, TagForm
from SWE573_Project.models import ArticleAnnotation, AnnotationModel, ArticleModel


@login_required(login_url="/login")
def annotate(request, article_id):
    form = TagForm()
    if request.method == 'POST':
        if 'annotateBtn' in request.POST:  # TODO: it should be button id for tag request - Ask Akın
            # TODO: Fix detail.js for selection
            selected_text = request.POST['annotation-section']
            print(selected_text)
            wiki_tags = TagForm(request.POST)  # TODO: Call wikidata tags
            selection_start = request.POST['startIndex']
            selection_end = request.POST['endIndex']
            tag = wiki_tags.save()  # TODO: Save tag annotation  # Save wikidata tags to tag

            ArticleAnnotation.objects.get_or_create(article=article_id,
                                                    target=selected_text,
                                                    tag=tag,
                                                    start_index=selection_start,
                                                    end_index=selection_end
                                                    )

            context = {
                'form': form
            }
    return render(request, 'pages/tag.html')


def save_annotation(self, article_id, tag_id, startIndex, endIndex):
    json = {
        "@context": "http://www.w3.org/ns/anno.jsonld",
        "id": annotation_uri,
        "type": "Annotation",
        "body": [{
            "type": "TextualBody",
            "value": tag_qid,
            "purpose": "tagging"
        }],
        "target": {
            # TODO: Get the URL of article id http://127.0.0.1:8000
            "source": "http://127.0.0.1:8000/details/" + article_slug,
            "selector": {
                "type": "TextPositionSelector",
                "start": startIndex,
                "end": endIndex
            }
        }
    }

    annotation = AnnotationModel(annotation_json=json)
    annotation.save()





# @login_required
# def comment(request):
#     if request.method == 'POST':
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.cleaned_data["CommentForm"]
#             a = ArticleAnnotation(comment)
#             a.save()
#
#             return render(request, 'details', {'form': CommentForm})
