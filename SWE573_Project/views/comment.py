# from django.views.generic import CreateView, ListView
# from django.views.generic.detail import DetailView
# from SWE573_Project.models import ArticleAnnotation
# from django.urls import reverse_lazy
# from SWE573_Project.forms.CommentForm import CommentForm, AnnotateForm
# from django.shortcuts import render, redirect
#
# class AddCommentView(CreateView):
#     model = ArticleAnnotation
#     form_class = CommentForm
#     template_name = 'add_comment.html'
#
#     def form_valid(self, form):
#         return super().form_valid(form)
#
#
#     def comment(request, article_id):
#         form = CommentForm()
#         activity = dict
#
#         if request.method == 'POST':
#             form = CommentForm(request.POST)
#             if form.is_valid():
#                 user_id = request.user.id
#                 cleaned_info = form.cleaned_data
#                 i = CommentForm.objects.filter(name=cleaned_info['name'])
#                 if not i.exists():
#                     i = form.save(user_id=user_id, article_id=article_id)
#                 else:
#                     i = i.first()
#                 article = ArticleAnnotation.objects.get(id=article_id)
#                 article.body.add(i)
#
#                 return redirect('comment', article.slug)
#
#         context = {
#             'form': form,
#         }
#         return render(request, 'pages/comment.html', context=context)
#
#
#
# class CommentListView(ListView):
#     model = ArticleAnnotation
#     template_name = 'comment.html'
#     context_object_name = 'comments'
#
# class AddAnnotateView(CreateView):
#     model = ArticleAnnotation
#     form_class = AnnotateForm
#     template_name = 'annotate.html'
#
#     def form_valid(self, form):
#         return super().form_valid(form)
#
#
#     def annotate(request, article_id):
#         form = AnnotateForm()
#         activity = dict
#
#         if request.method == 'POST':
#             form = AnnotateForm(request.POST)
#             if form.is_valid():
#                 user_id = request.user.id
#                 cleaned_info = form.cleaned_data
#                 i = AnnotateForm.objects.filter(name=cleaned_info['name'])
#                 if not i.exists():
#                     i = form.save(user_id=user_id, article_id=article_id)
#                 else:
#                     i = i.first()
#                 article = ArticleAnnotation.objects.get(id=article_id)
#                 article.body.add(i)
#
#                 return redirect('comment', article.slug)
#
#         context = {
#             'form': form,
#         }
#         return render(request, 'pages/annotate.html', context=context)
#
# class AnnotateListView(ListView):
#     model = ArticleAnnotation
#     template_name = 'comment.html'
#     context_object_name = 'annotates'