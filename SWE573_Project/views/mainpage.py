from django.shortcuts import render
from SWE573_Project.models import ArticleModel
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, TrigramSimilarity, TrigramDistance
from account.models import CustomUserModel

@login_required(login_url="/login")
def mainpage(request):
    search = request.GET.get('search')
    if search:
        # articles = ArticleModel.objects.order_by('-id')

        vector = SearchVector('title', weight='A') + SearchVector('keywords', weight='B') + SearchVector('body', weight='C')
        query2 = SearchQuery(search)

        user_name = request.user.username
        user = CustomUserModel.objects.get(username=user_name)
        user.searchHistory = search
        user.save()

        object_list = ArticleModel.objects.annotate(distance=TrigramDistance('body', query2)).filter(distance__lte=0.3).order_by('distance')
        object_list = ArticleModel.objects.annotate(search=SearchVector('title','keywords','body'),).filter(search=SearchQuery(search))
        object_list = ArticleModel.objects.annotate(rank=SearchRank(vector, query2, cover_density=True)).order_by('-rank')
        articles = object_list

        paginator = Paginator(articles, 10)
        page = request.GET.get('page', 1)
        return render(request, 'pages/homepage.html', context={
            'articles': paginator.get_page(page)

        })


    else:
        user_name = request.user.username
        user = CustomUserModel.objects.get(username=user_name)
        search = user.searchHistory

        # articles = ArticleModel.objects.order_by('-id')

        vector = SearchVector('title', weight='A') + SearchVector('keywords', weight='B') + SearchVector('body', weight='C')
        query2 = SearchQuery(search)

        object_list = ArticleModel.objects.annotate(distance=TrigramDistance('body', query2)).filter(distance__lte=0.3).order_by('distance')
        object_list = ArticleModel.objects.annotate(search=SearchVector('title','keywords','body'),).filter(search=SearchQuery(search))
        object_list = ArticleModel.objects.annotate(rank=SearchRank(vector, query2, cover_density=True)).order_by('-rank')
        articles = object_list


        paginator = Paginator(articles, 10)
        page = request.GET.get('page', 1)
        return render(request, 'pages/homepage.html', context={
            'articles': paginator.get_page(page)
            

        })
