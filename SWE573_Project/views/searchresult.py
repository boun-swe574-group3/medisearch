from django.shortcuts import render
from SWE573_Project.models import ArticleModel
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, TrigramSimilarity, TrigramDistance
from django.shortcuts import redirect


@login_required(login_url="/login")
def search(request):
    search = request.GET.get('search')
    # articles = ArticleModel.objects.order_by('-id')

    # vector = SearchVector('title', weight='A') + SearchVector('keywords',
    #                                                           weight='B') + SearchVector('body', weight='C')
    # query2 = SearchQuery(search)

    # object_list = ArticleModel.objects.annotate(distance=TrigramDistance(
    #     'body', query2)).filter(distance__lte=0.3).order_by('distance')
    # object_list = ArticleModel.objects.annotate(search=SearchVector(
    #     'title', 'keywords', 'body'),).filter(search=SearchQuery(search))
    # object_list = ArticleModel.objects.annotate(rank=SearchRank(
    #     vector, query2, cover_density=True)).order_by('-rank')
    # #articles = object_list

    # paginator = Paginator(object_list, 10)
    # page = request.GET.get('page')
    # page_obj = paginator.get_page(page)

    # return render(request, 'pages/searchresult.html', context={
    #     'articles': page_obj
    # })

    #search = request.GET.get('search')
    query = request.GET.get('search', '')
    articles = ArticleModel.objects.order_by('-id')
    # query none or empty
    search_result = []
    if query:
        search_result = articles.filter(
            Q(title__icontains=query) |
            Q(body__icontains=query) |
            Q(keywords__icontains=query)
        ).distinct()
    paginator = Paginator(search_result, 10)
    page = request.GET.get('page', 1)

    return render(request, 'pages/searchresult.html', context={
        'search_result': paginator.get_page(page), 'query': query
    })
