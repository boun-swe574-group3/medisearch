from django.shortcuts import render, redirect
from SWE573_Project.forms.TagForm import TagForm
from SWE573_Project.models import ArticleModel, TagModel
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView


class DetailsTag(CreateView):
    model = TagModel
    form_class = TagForm
    template_name = 'details.html'
    #fields = '__all__'

    @login_required(login_url="/login")
    def DetailsTag(request, article_id):
        form = TagForm()
        if request.method == 'POST':
            form = TagForm(request.POST)
            if form.is_valid():
                user_id = request.user.id
                cleaned_info = form.cleaned_data
                i = TagModel.objects.filter(name=cleaned_info['name'])
                if not i.exists():
                    i = form.save(user_id=user_id, article_id=article_id)
                else:
                    i = i.first()
                article = ArticleModel.objects.get(id=article_id)
                article.tags.add(i)
                return redirect('details', article.slug)

        context = {
            'form': form
        }
        return render(request, 'pages/details.html', context=context)
