import json

from django.shortcuts import render, get_object_or_404, redirect

# from SWE573_Project.forms.CommentForm import AnnotationForm
from SWE573_Project.forms import AnnotationForm
from SWE573_Project.models import ArticleModel, TagModel, AnnotationModel, AnnotateMed, ArticleAnnotation, Activity, \
    ActivityModel
from django.contrib.auth.decorators import login_required
from SWE573_Project.forms.TagForm import TagForm
from django.views.generic import View
import datetime
from account.models import CustomUserModel


class details(View):

    def get(self, request, *args, **kwargs):
        form = AnnotationForm()
        article = get_object_or_404(ArticleModel, slug=self.kwargs['slug'])
        tags = article.tags.filter(articleannotation__isnull=True)
        annotation_tags = article.tags.filter(articleannotation__isnull=False)
        # obj = ArticleAnnotation.objects.values_list()
        annotations = article.article_ann.all()
        values = annotations.values_list()
        print(values)
        valuesAnn = []
        for v in values:
            # print(v[4])
            valuesAnn.append(v[4])
        print(valuesAnn)

        return render(request, 'pages/details.html', context={
            'article': article, 'form': form, 'tags': tags, 'annotation_tags': annotation_tags, 'values': valuesAnn
        })

    def post(self, request, *args, **kwargs):
        article = get_object_or_404(ArticleModel, slug=self.kwargs['slug'])
        article_id = article.id
        article_slug = article.slug
        form = AnnotationForm(request.POST)

        if form.is_valid():
            user_id = request.user.id
            cleaned_info = form.cleaned_data
            # i = ArticleAnnotation.objects.filter(name=cleaned_info['name'])
            # if not i.exists():
            a = form.save()

            article.article_ann.add(a)
            # else:
            #     i = i.first()
            # article = ArticleModel.objects.get(id=article_id)
            # article.annotation.add(i)
            user = CustomUserModel.objects.get(id=user_id)
            json_annotation = {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "id": "http://18.222.146.15:80/annotations/",
                "type": "Annotation",
                "body": [{
                    "type": "TextualBody",
                    "value": a.target.url,
                    "purpose": "tagging",
                }],
                "target": {
                    "source": "http://18.222.146.15:80/details/" + article_slug,
                    "selector": {
                        "type": "TextPositionSelector",
                        "start": a.start_index,
                        "end": a.end_index,
                    }
                }
            }

            # print(json_annotation)  # single quotes "with backslash in db"
            # print(type(json_annotation))
            jsonld = json.dumps(json_annotation)
            print(jsonld)
            # print(json_annotation['purpose'])

            r = json_annotation
            r = json.dumps(r)
            # print(r)  # double quotes "with backslash in db "
            loaded_r = json.loads(r)
            # print(loaded_r)

            b = AnnotationModel.AnnotateMed.objects.create(
                annotation_json=json_annotation)
            b.save()
            article.annotation.add(b)
            # print(article.annotation.get())

            # print(article.annotation)
            # print(type(article.annotation))

            # print(a.target)

            # ACTIVITY SAVER FOR ANNOTATION
            createdTime = datetime.datetime.now()
            message = f"{user} annotated the article '{article}' with the tag of '{a.target}' at {createdTime}"
            articleLink = f"http://18.222.146.15:80/details/{article.slug}"
            userLink = f"http://18.222.146.15:80/profiles/{user.profile.id}"

            json_activity = {
                "@context": "https://www.w3.org/ns/activitystreams",
                "summary": message,
                "type": "Add",
                "published": str(createdTime),
                "actor": {
                    "type": "Person",
                    "name": str(request.user),
                    "url": userLink
                },
                "object": {
                    "type": "Page",
                    "name": str(a.target),
                    "url": str(a.target.url)
                },
                "target": {
                    "type": "Note",
                    "name": article.title,
                    "content": article.body,
                    "attributedTo": articleLink
                }
            }

            # print(type(json_activity))
            #
            activity = json.dumps(json_activity)
            # print(type(activity))  # double quotes ""
            # loaded_activity = json.loads(activity)
            # print(json_activity['actor']['url'])

            newAnnotationactivity = ActivityModel.Activity.objects.create(
                activity_JSON=json_activity)
            newAnnotationactivity.save()
            user.profile.activities.add(newAnnotationactivity)
            # print(ActivityModel.Activity.objects.first())

            #message = f"{user} tagged {article} with the tag of {i}"
            #created = datetime.datetime.now

            # json_activity = {
            #     "@context": "https://www.w3.org/ns/activitystreams",
            #     "summary": 'message',
            #     "type": "Create",
            #     "id": "http://www.test.example/activity/1",
            #     "actor": "http://example.org/profiles/joe",
            #     "object": "http://example.com/notes/1",
            #     "published": 'created'
            # }

            return redirect('details', article.slug)

        # response = json.dumps(activity)
        # response = serializers.serialize('json', activity)
        # print(f"{response} IS THE RESPONSE")
        #activity = Activity(user=user, activity_JSON=json)
        # activity.save()

        # json = {
        #     "@context": "https://www.w3.org/ns/activitystreams",
        #     "summary": "{} followed {}".format(self.getOwnerName(), activity_target_name),
        #     "type": "Follow",
        #     "published": self.getCurrentTimeAsISO(),
        #     "actor": {
        #         "type": "Person",
        #         "id": self.getOwnerURL(),
        #         "name": self.getOwnerName(),
        #         "url": self.getOwnerURL()
        #     },
        #     "object": {
        #         "id": activity_target_url,
        #         "type": activity_target_type,
        #         "url": activity_target_url,
        #         "name": activity_target_name
        #     }
        # }

        # activity = Activity(
        #     user_id=self.user_id,
        #     activity_type=2,
        #     target_type=1,
        #     target_id=target_id,
        #     activity_JSON=json
        # )
        # activity.save()


# @login_required(login_url="/login")
# def details(request, slug):
#     article = get_object_or_404(ArticleModel, slug=slug)
#     article_id = article.id
#     print(article.id)
#     form = TagForm()
#     if request.method == 'POST':
#         form = TagForm(request.POST)
#         print(form)
#         if form.is_valid():
#             user_id = request.user.id
#             cleaned_info = form.cleaned_data
#             i = TagModel.objects.filter(name=cleaned_info['name'])
#             if not i.exists():
#                 i = form.save(user_id=user_id, article_id=article_id)
#             else:
#                 i = i.first()
#             article = ArticleModel.objects.get(id=article_id)
#             article.tags.add(i)
#             return redirect('details', article.slug)

#     return render(request, 'pages/details.html', context={
#         'article': article,
#     })
