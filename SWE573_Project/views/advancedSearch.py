from django.shortcuts import render
from SWE573_Project.models import ArticleModel
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from account.models import CustomUserModel
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.shortcuts import render
from plotly.offline import plot
import plotly.graph_objects as go


@login_required(login_url="/login")
def advancedSearch(request):
    x = datetime.datetime.now()
    from_date = 1980
    to_date = x.year
    diff = to_date - from_date
    date_list = [from_date + i for i in range(diff + 1)]

    advancedSearchText = request.GET.get('advancedSearch')

    fromYear = request.GET.get('fromYear')
    toYear = request.GET.get('toYear')

    if fromYear:
        fromYear = fromYear
    else:
        fromYear = from_date

    if toYear:
        toYear = toYear
    else:
        toYear = to_date


    articles = ArticleModel.objects.order_by('-id')
    order = request.GET.get('orderArticles')
    if order == "newestFirst":
        articles = ArticleModel.objects.order_by('-publish_date')
    elif order == 'oldestFirst':
        articles = ArticleModel.objects.order_by('publish_date')

    tagAnnotationChoice = request.GET.get('tagAnnotationChoice')

    if advancedSearchText:
        if fromYear:
            if toYear:
                if tagAnnotationChoice == "includeTag":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeAnnotation":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeBoth":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText) |
                         Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyTag":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyAnnotation":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyBoth":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "none":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                else:
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()

            else:
                if tagAnnotationChoice == "includeTag":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeAnnotation":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeBoth":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyTag":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyAnnotation":
                    articles = articles.filter(
                        (Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyBoth":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                elif tagAnnotationChoice == "none":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
                else:
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__gte=fromYear)
                    ).distinct()
        else:
            if toYear:
                if tagAnnotationChoice == "includeTag":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeAnnotation":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "includeBoth":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyTag":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyAnnotation":
                    articles = articles.filter(
                        (Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "onlyBoth":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                elif tagAnnotationChoice == "none":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()
                else:
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)) &
                        Q(publish_date__year__lte=toYear)
                    ).distinct()

            else:
                if tagAnnotationChoice == "includeTag":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "includeAnnotation":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(articleannotation__body__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "includeBoth":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText)|
                         Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "onlyTag":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "onlyAnnotation":
                    articles = articles.filter(
                        (Q(articleannotation__body__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "onlyBoth":
                    articles = articles.filter(
                        (Q(tags__name__icontains=advancedSearchText) |
                         Q(articleannotation__body__icontains=advancedSearchText))
                    ).distinct()
                elif tagAnnotationChoice == "none":
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText))
                    ).distinct()
                else:
                    articles = articles.filter(
                        (Q(title__icontains=advancedSearchText) |
                         Q(body__icontains=advancedSearchText) |
                         Q(keywords__icontains=advancedSearchText))
                    ).distinct()

        user_name = request.user.username
        user = CustomUserModel.objects.get(username=user_name)
        user.searchHistory = advancedSearchText
        user.save()

    else:
        if fromYear:
            if toYear:
                articles = articles.filter(
                    Q(publish_date__year__gte=fromYear) &
                    Q(publish_date__year__lte=toYear)
                ).distinct()
            else:
                articles = articles.filter(
                    Q(publish_date__year__gte=fromYear)
                ).distinct()
        else:
            if toYear:
                articles = articles.filter(
                    Q(publish_date__year__lte=toYear)
                ).distinct()
            else:
                messages.add_message(request, messages.INFO, 'You should type a text!')


    # if advancedSearchText:
    #     if fromYear:
    #         if toYear:
    #             articles = articles.filter(
    #                 (Q(title__icontains=advancedSearchText) |
    #                  Q(body__icontains=advancedSearchText) |
    #                  Q(keywords__icontains=advancedSearchText)) &
    #                 Q(publish_date__year__gte=fromYear) &
    #                 Q(publish_date__year__lte=toYear)
    #             ).distinct()
    #         else:
    #             articles = articles.filter(
    #                 (Q(title__icontains=advancedSearchText) |
    #                  Q(body__icontains=advancedSearchText) |
    #                  Q(keywords__icontains=advancedSearchText)) &
    #                 Q(publish_date__year__gte=fromYear)
    #             ).distinct()
    #     else:
    #         articles = articles.filter(
    #             (Q(title__icontains=advancedSearchText) |
    #              Q(body__icontains=advancedSearchText) |
    #              Q(keywords__icontains=advancedSearchText))
    #         ).distinct()
    #
    #     user_name = request.user.username
    #     user = CustomUserModel.objects.get(username=user_name)
    #     user.searchHistory = advancedSearchText
    #     user.save()
    #
    # else:
    #     messages.add_message(request, messages.INFO, 'You should type a text!')

    # page = request.GET.get('page')

    """ 
    Graph part of Advanced Search
    """
    counter1 = 0
    counter2 = 0
    counter3 = 0
    counter4 = 0
    counter5 = 0

    for article in articles:
        if article.publish_date is not None:
            if 1990 > article.publish_date.year >= 1980:
                counter1 += 1
            elif 2000 > article.publish_date.year >= 1990:
                counter2 += 1
            elif 2010 > article.publish_date.year >= 2000:
                counter3 += 1
            elif 2020 > article.publish_date.year >= 2010:
                counter4 += 1
            elif article.publish_date.year >= 2020:
                counter5 += 1
        else:
            counter5 += 1

    x = ["1980-1990", "1990-2000", "2000-2010", "2010-2020", "2020-To Date"]
    y = [counter1, counter2, counter3, counter4, counter5]

    # List of graph objects for figure.
    # Each object will contain on series of data.
    graphs = []

    # Adding linear plot of y1 vs. x.
    # graphs.append(
    #     go.Scatter(x=x, y=y1, mode='lines', name='Line y1')
    # )

    # Adding scatter plot of y2 vs. x.
    # Size of markers defined by y2 value.
    # graphs.append(
    #     go.Scatter(x=x, y=y2, mode='markers', opacity=0.8,
    #                marker_size=y2, name='Scatter y2')
    # )

    # Adding bar plot of y3 vs x.
    graphs.append(
        go.Bar(x=x, y=y, name='Articles on Decades')
    )

    # Setting layout of the figure.
    layout = {
        'title': '',
        'xaxis_title': 'Decades',
        'yaxis_title': 'Number of Articles',
        'height': 280,
        'width': 840,
    }

    # Getting HTML needed to render the plot.
    plot_div = plot({'data': graphs, 'layout': layout},
                    output_type='div')


    return render(request, 'pages/advancedsearch/advancedSearch.html', context={
        'advancedSearch': advancedSearchText, 'fromYear': fromYear, 'toYear': toYear, 'counter': articles.__len__(),
        'dateList': date_list, 'articles': articles, "query": advancedSearchText, 'plot_div': plot_div,
    })
