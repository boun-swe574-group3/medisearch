from django.test import TestCase, Client,TransactionTestCase
from SWE573_Project.models import ArticleModel,ReportModel,TagModel, ActivityModel, ArticleAnnotation
from account.models import CustomUserModel
from django.db.utils import IntegrityError
from django.contrib.auth import get_user_model


class ArticleTestCase(TestCase):
    def setUp(self):
        ArticleModel.objects.create(title='article title', body="article body", authors='author', pmid='1234', keywords='keyword1', slug='testslug')
        ArticleModel.objects.create(title='article 1', body='body 1', authors='author1', pmid='5678', keywords='keyword1', slug='testslug1')

    def test_article_is_created_properly(self):
        article = ArticleModel.objects.get(slug='testslug')
        self.assertEqual(article.title, 'article title')
        self.assertEqual(article.body, 'article body')
        self.assertEqual(article.authors, 'author')
        self.assertEqual(article.pmid, '1234')
        self.assertEqual(article.keywords, 'keyword1')
    
    def test_same_article(self):
        with self.assertRaises(IntegrityError):
            ArticleModel.objects.create(title="article title", body="article body", authors='author', pmid='1234', keywords='keyword1', slug='testslug')

    def test_unique_field_pmid(self):
        with self.assertRaises(IntegrityError):
            ArticleModel.objects.create(title='article 2', body='body 2', authors='author2', pmid='5678', keywords='keyword2', slug='testslug')

class TagTestCase(TestCase):

    def setUp(self):
        TagModel.objects.create(name='testtag', url='testurl.com')

    def test_tag_is_created_properly(self):
        tag = TagModel.objects.get(name='testtag')
        self.assertEqual(tag.name, 'testtag')
        self.assertEqual(tag.url, 'testurl.com')

class ReportTestCase(TestCase):

    def setUp(self):
        ReportModel.objects.create(message='report test message')

    def test_report_created_properly(self):
        report = ReportModel.objects.get(message='report test message')
        self.assertEqual(report.message, 'report test message')

class UserTestCase(TestCase):

    User = get_user_model()

    def setUp(self):
        self.user = CustomUserModel.objects.create_user(username='testuset', email='test@test.com', password='testpassword')

    def test_user(self):
        self.assertEqual(self.user.email, 'test@test.com' )

    def test_login_url(self):
        login_url = "/login"
        data = {"username":"testuset", "password":"testpassword"}
        response = self.client.post(login_url, data, follow=True)
        status_code = response.status_code
        self.assertEqual(status_code, 200)


class AnnotationTestCase(TestCase):

    def setUp(self):
        tag = TagModel.objects.create(name='testtag', url='testurl.com')
        ArticleAnnotation.objects.create(target=tag, start_index=1, end_index=3, body="some selected text")

    def test_annotation_is_created(self):
        annotation = ArticleAnnotation.objects.get(body="some selected text")
        tag = TagModel.objects.get(name='testtag')
        self.assertEqual(annotation.target, tag)
        self.assertEqual(annotation.start_index, 1)
        self.assertEqual(annotation.end_index, 3)
        self.assertEqual(annotation.body, "some selected text")



class ProfileTestCase(TestCase):

    def test_user_profile(self):
        newuser = CustomUserModel.objects.create_user(username='testuser', email='test@test.com',
                                                      password='testpassword')
        newuser.save()

        self.assertTrue(
            hasattr(newuser, 'profile')
        )

class ActivityTestCase(TestCase):

    def setUp(self):
        ActivityModel.Activity.objects.create(activity_JSON={
            "@context": "https://www.w3.org/ns/activitystreams",
            "summary": "message",
            "type": "Add",
            "published": "str(createdTime)",
            "actor": {
                "type": "Person",
                "name": "str(request.user)",
                "url": "userLink"
            }
        })

    def test_if_activity_created(self):
        activity = ActivityModel.Activity.objects.get(activity_JSON={
            "@context": "https://www.w3.org/ns/activitystreams",
            "summary": "message",
            "type": "Add",
            "published": "str(createdTime)",
            "actor": {
                "type": "Person",
                "name": "str(request.user)",
                "url": "userLink"
            }
        })
        self.assertEqual(activity.activity_JSON, {
            "@context": "https://www.w3.org/ns/activitystreams",
            "summary": "message",
            "type": "Add",
            "published": "str(createdTime)",
            "actor": {
                "type": "Person",
                "name": "str(request.user)",
                "url": "userLink"
            }
        })

#Signup Form Test
class FormResponse(TestCase):
    def test_signup_form_ok(self):
        client = Client()
        url = '/createaccount'
        fields = {'username': 'gulcaa',
                  'first_name': 'gulsah',
                  'last_name': 'keskin',
                  'email': 'gulsah@gg.com',
                  'password1': 'APap1234',
                  'password2': 'APap1234'}
        response = client.post(url, fields)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/login')


#Advanced Search Test
class AdvancedSearchTest(TestCase):
    def setUp(self):
        ArticleModel.objects.create(title='covid', body="article body", authors='author', pmid='1234',
                                        keywords='keyword1', slug='testslug')
        ArticleModel.objects.create(title='article 1', body='cancer', authors='author1', pmid='5678',
                                        keywords='keyword2', slug='testslug1')
    def test_advanced_search_title(self):
        self.assertQuerysetEqual(ArticleModel.objects.filter(title__icontains='covid'), ["<ArticleModel: covid>"])

    def test_advanced_search_body(self):
        self.assertQuerysetEqual(ArticleModel.objects.filter(body__icontains='cancer'), ["<ArticleModel: article 1>"])

    def test_advanced_search_keywords(self):
        self.assertQuerysetEqual(ArticleModel.objects.filter(keywords__icontains='keyword1'), ["<ArticleModel: covid>"])


