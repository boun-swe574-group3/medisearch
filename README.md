# AnnotateMed Guidelines

[Deployment Link](http://18.222.146.15/)

## How to install and run the project for Mac users

```bash
$ cp .env.example .env
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

## How to install and run the project for Windows users

```bash
$ cp .env.example .env
$ python -m venv venv
$ cd venv
$ .\Scripts\activate
$ cd ..
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```

## How to run on Docker

```bash
$ docker rmi -f $(docker images -a -q)
$ docker container prune
$ docker-compose up --build
$ docker exec -it medisearch_web_1 /bin/bash
```

## How to export database

```bash
$ pg_dump -U <db_username> <db_name> -h <host> -t <table_name> > exported_data.sql
```

## How to import database

```bash
$ psql -U <db_username> <db_name> -h <host> < exported_data.sql
```
