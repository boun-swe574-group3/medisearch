#!/bin/sh

ssh -o StrictHostKeyChecking=no ec2-user@18.118.23.140<< 'ENDSSH'
  cd /home/ec2-user/medisearch
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull registry.gitlab.com/boun-swe574-group3/medisearch
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH