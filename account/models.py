from django.db import models
from django.contrib.auth.models import AbstractUser
from PIL import Image
from django.utils import timezone
from SWE573_Project.models.ActivityModel import Activity


class CustomUserModel(AbstractUser):
    avatar = models.ImageField(upload_to='avatar/', default='default.jpg')
    searchHistory = models.CharField(max_length=10000, default= "SearchText",blank=True)
    

    class Meta:
        db_table = 'user'
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.username

class Profile(models.Model):
    user = models.OneToOneField(CustomUserModel, on_delete=models.CASCADE)
    shortbio = models.TextField(default='', verbose_name='shortbio')   
    location = models.CharField(null=True, max_length=100, verbose_name='location')
    following = models.ManyToManyField(CustomUserModel, related_name='following', blank=True)
    follower = models.ManyToManyField(CustomUserModel, related_name='follower', blank=True)
    created = models.DateTimeField(default=timezone.now)
    activities = models.ManyToManyField(Activity, related_name='activity', null=True, blank=True)

    def __str__(self):
        return f'{self.user.username}'

    class Meta:
        ordering = ('-created',)
