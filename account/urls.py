from django.urls import path

from SWE573_Project import views
from account.views import logoutView
from account.views import changepassword
from account.views import profilesettings
from account.views import createnewuser
from account.views.profile import FollowingListView, FollowerListView, ProfileListView, profile, ProfileDetailView, follow_unfollow


urlpatterns = [
    path('logoutView', logoutView, name='logout'),
    path('changepassword', changepassword, name='changepassword'),
    path('profilesettings', profilesettings, name='profilesettings'),
    path('createaccount', createnewuser, name='createaccount'),
    path('profile', profile, name='profile'),
    path('profiles', ProfileListView.as_view(template_name='pages/main.html'), name='profile-list'),
    path('profiles/follow', follow_unfollow, name='follow'),
    path('profiles/<int:pk>', ProfileDetailView.as_view(template_name='pages/profiledetail.html'), name='profile-detail'),
    path('following/', FollowingListView.as_view(template_name='pages/following.html'), name='followings'),
    path('follower/', FollowerListView.as_view(template_name='pages/follower.html'), name='followers'),
    # path('annotations/', views.annotations, name='annotations')
]