from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from account.forms import ProfileSettingsForm
from django.views.generic import ListView, DetailView
from account.models import Profile
from django.http import HttpResponseRedirect

@login_required
def profile(request):
    if request.method == 'POST':
        userform = ProfileSettingsForm(request.POST, instance=request.user)
        profileform = ProfileSettingsForm(request.POST, request.FILES, instance=request.user.profile)

        if userform.is_valid() and profileform.is_valid():
            userform.save()
            profileform.save()
            messages.success(request, f'Your account has been updated.')
            return redirect('profile')

    else:
        userform = ProfileSettingsForm(instance=request.user)
    context = {
        'userform':userform,
    }
    return render(request, 'pages/profile.html', context)

def follow_unfollow(request):
    if request.method=="POST":
        my_profile = Profile.objects.get(user=request.user)
        print(my_profile)
        pk = request.POST.get('profile_pk')
        print(pk)
        obj = Profile.objects.get(pk=pk)
        print(obj)

        if obj.user in my_profile.following.all() and my_profile.user in obj.follower.all():
            my_profile.following.remove(obj.user)
            obj.follower.remove(my_profile.user)
        else:
            my_profile.following.add(obj.user)
            obj.follower.add(my_profile.user)
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect('profile-list')

class ProfileListView(ListView):
    model = Profile
    template_name = 'pages/main.html'
    context_object_name = 'profiles'

    # exclude current user
    def get_query(self):
        return Profile.objects.all().exclude(user=self.request.user)

class ProfileDetailView(DetailView):
    model = Profile
    template_name = 'pages/profiledetail.html'

    def get_object(self, **kwargs):
        pk = self.kwargs.get('pk')
        view_profile = Profile.objects.get(pk=pk)
        return view_profile

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        view_profile = self.get_object()
        my_profile = Profile.objects.get(user=self.request.user)
        if view_profile.user in my_profile.following.all():
            follow = True
        else:
            follow = False
        context["follow"] = follow
        return context
    
class FollowingListView(ListView):
    model = Profile
    template_name = 'pages/following.html'
    context_object_name = 'followings'

    # exclude current user
    def get_query(self):
        return Profile.objects.all().exclude(user=self.request.user)

class FollowerListView(ListView):
    model = Profile
    template_name = 'pages/follower.html'
    context_object_name = 'followers'

    # exclude current user
    def get_query(self):
        return Profile.objects.all().exclude(user=self.request.user)